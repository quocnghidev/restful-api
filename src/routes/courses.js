const express = require("express");
const Router = express.Router();
const Controller = require("../controllers/CourseController");

// [GET] /courses/
Router.get("/", Controller.getAll);

// [GET] /courses/:id
Router.get("/:id", Controller.getOne);

// [DELETE] /courses/
Router.delete("/", Controller.deleteAll);

// [DELETE] /courses/:id
Router.delete("/:id", Controller.deleteOne);

// [POST] /courses/
Router.post("/", Controller.create);

// [PUT] /courses/:id
Router.put("/:id", Controller.replace);

// [PATCH] /courses/:id
Router.patch("/:id", Controller.update);

module.exports = Router;
