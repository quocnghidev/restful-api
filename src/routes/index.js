const courses = require("./courses");

const init = (app) => {
  app.use("/courses", courses);
};

module.exports = { init };
