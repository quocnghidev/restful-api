const Course = require("../models/Course");

class CourseController {
  // [GET] /courses/
  async getAll(req, res) {
    try {
      const data = await Course.find({});
      res.json({
        status: 200,
        message: "Danh sách khóa học",
        courses: data,
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  }

  // [GET] /courses/:id
  async getOne(req, res) {
    try {
      const data = await Course.findById(req.params.id);
      if (data)
        return res.json({
          status: 200,
          message: "Khóa học",
          course: data,
        });
      res.json({
        status: 404,
        message: `Không tìm thấy khóa học có id: ${req.params.id}`,
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  }

  // [DELETE] /courses/
  async deleteAll(req, res) {
    try {
      await Course.deleteMany({});
      res.json({
        status: 200,
        message: "Đã xóa thành công",
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  }

  // [DELETE] /courses/:id
  async deleteOne(req, res) {
    try {
      const result = await Course.findByIdAndDelete(req.params.id);
      if (result)
        return res.json({
          status: 200,
          message: "Đã xóa thành công",
        });
      res.json({
        status: 404,
        message: "Không tìm thấy khóa học để xóa",
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  }

  // [POST] /courses/
  async create(req, res) {
    try {
      const result = await Course.create(req.body);
      res.json({
        status: 200,
        message: "Tạo thành công",
        course: result,
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  }

  // [PUT] /courses/:id
  async replace(req, res) {
    try {
      const result = await Course.findOneAndReplace(
        { _id: req.params.id },
        req.body
      );
      if (result)
        return res.json({
          status: 200,
          message: "Chỉnh sửa thành công",
        });
      res.json({
        status: 404,
        message: "Không tìm thấy khóa học để chỉnh sửa",
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  }

  // [PATCH] /courses/:id
  async update(req, res) {
    try {
      const result = await Course.findOneAndUpdate(
        { _id: req.params.id },
        req.body
      );
      if (result)
        return res.json({
          status: 200,
          message: "Chỉnh sửa thành công",
        });
      res.json({
        status: 404,
        message: "Không tìm thấy khóa học để chỉnh sửa",
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  }
}

module.exports = new CourseController();
