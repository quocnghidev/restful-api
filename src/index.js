const express = require("express");
const app = express();
const PORT = 3000;
const mongoDB = require("./config/dbconfig");
const routes = require("./routes");

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

mongoDB.connect();

routes.init(app);

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
