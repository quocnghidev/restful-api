const mongoose = require("mongoose");

const connect = async () => {
  try {
    await mongoose.connect("mongodb://localhost:27017/example");
    console.log("Connect to DB successfully!");
  } catch (error) {
    console.log(error);
  }
};

module.exports = { connect };
